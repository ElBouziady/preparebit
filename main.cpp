#include "mainwindow.h"
#include <QApplication>
#include <QFile>
#include <QTextStream>

#include <iostream>
#include <opencv2/opencv.hpp>

using namespace cv;
using namespace std;

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    QFile file("/home/boukary/Desktop/Lotfi/BIT.txt");
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        return 0 ;

    QTextStream in(&file);
    while (!in.atEnd()) {
        QString line = in.readLine();
        cout<<line.toStdString()<<endl;
        QStringList linelist = line.split(" ");

        Mat image = imread(linelist[0].toStdString());
        int nbr_vec = linelist[1].toInt();

        for (int i = 2; i < linelist.size()-1; i+=5) {
            string category = linelist[i].toStdString();
            int x = linelist[i+1].toInt();
            int y = linelist[i+2].toInt();
            int w = linelist[i+3].toInt()-10;
            int h = linelist[i+4].toInt()-10;
            //imshow("image",image(Rect(x,y,w,h)));
            imwrite(format("/home/boukary/Desktop/Lotfi/%s/%i%s",category.c_str(),i,linelist[0].split("/").at(6).toStdString().c_str()),image(Rect(x,y,w,h)));
            //waitKey(0);
        }

    }

    return a.exec();
}
